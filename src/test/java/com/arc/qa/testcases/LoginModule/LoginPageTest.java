package com.arc.qa.testcases.LoginModule;

import java.io.IOException;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

//import com.arm.arc.util.capturePage;
import com.relevantcodes.extentreports.ExtentReports;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import com.arm.arc.base.TestBase;
import com.arm.arc.pages.HomePage;
import com.arm.arc.pages.LoginPage;
import com.arm.arc.pages.NewEZCommit;
import com.arm.arc.pages.VCModule;
//Three test cases are run in LoginPage, so browser will be opened three different times
//this is testing
//Test #1: Test that the Page Title is AutoRABIT Login.
//Test #2: Test that you are able to login
public class LoginPageTest extends TestBase {
	LoginPage loginPage;
	HomePage homePage;
	VCModule vcmodule;
	NewEZCommit newezcommit;

	//constructor calls TestBase method
	public LoginPageTest() {
		super();
	}


	//will call initialization method from TestBase class so all the properties are defined.
	@BeforeTest
	public void setUp() {
		//sets driver, manages windows, and timeouts
		initialization();
		loginPage = new LoginPage();
		 extent = new ExtentReports(System.getProperty("user.dir") + "/test-output/Extent.html", true);
	}
	
	//Testing to see if page title matches & explicitly defining test execution priority
	//otherwise it priorities will be run alphabetically.
	@Test(priority=1) 
	public void loginPageTitleTest() throws IOException {
		test = extent.startTest("loginPageTitleTest");// Start this test
		String title = loginPage.validateLoginPageTitle();
		
		Assert.assertEquals(title, "AutoRABIT Login","Title Mismatch");
		//capturePage.takeScreenshotAtEndOfTest();
		//test.log(LogStatus.FAIL, result.getThrowable());
		//test.log(LogStatus.FAIL, "Snapshot below: " + capturePage.takeScreenshotAtEndOfTest());
		
	}
	//Tests if login credentials are valid. Taken from config.properties file
	//& explicitly defining test execution priority //otherwise it priorities will be run alphabetically.
	//Access to prop object due because it is inherited from TestBase class.
	@Test(priority=4)
	public void loginTest() throws InterruptedException {
		homePage = loginPage.login();
		vcmodule=(VCModule) homePage.navigateToParticularModule("VC"); 
		newezcommit=(NewEZCommit) vcmodule.ClickOnActionInModule("ezcommit"); 
		//String retValue = newezcommit.PerformNewEZCommit(); 
		 newezcommit.ApproveALabel("PV-Commit1634451653", "Approve");
		System.out.print("retValue");
		  
		 	}
 
	
	@Test(priority = 2)
	public void verifyForgotPasswordWithProperEmail() {
		loginPage = loginPage.verifyForgotPasswordWithProperValidEmail();
	}

	@Test(priority = 3,description="testing desc")
	@Severity(SeverityLevel.NORMAL)
	@Description("this is allure desc")
	public void verifyForgotPasswordWithInvalidEmail() {
		loginPage = loginPage.verifyForgotPasswordWithInValidEmail();
	}
	
	 @AfterMethod
	    public void getResult(ITestResult result) throws IOException
	    {
	        if(result.getStatus() == ITestResult.FAILURE)
	        {
	            /*String screenShotPath = capturePage.takeScreenshotAtEndOfTest();
	            test.log(LogStatus.FAIL, result.getThrowable());
	            test.log(LogStatus.FAIL, "Snapshot below: " + test.addScreenCapture(screenShotPath)); */
	        	//test.log(LogStatus.FAIL, "Snapshot below: " + test.addScreenCapture(capturePage.takeScreenshotAtEndOfTest()));
	        }
	        extent.endTest(test);
	    }
	     

	@AfterTest
	public void tearDown() {
		driver.close();
        extent.flush();
        extent.close();
		driver.quit();
	}
}
