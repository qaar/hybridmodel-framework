package com.arm.arc.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.arm.arc.util.TestUtil;
import com.arm.arc.util.WebEventListener;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;

public class TestBase {

	public static WebDriver driver;
	public static Properties prop;
	public static Properties locat;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	
	public ExtentReports extent; //Making it public as it can be accessible in every package
	public ExtentTest test;	

	//Placing config.properties contents to ip variable and loading it using Properties variable.
	public TestBase(){
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(System.getProperty("user.dir") + "/src/main/java/com/arm/arc/config/config.properties");
			prop.load(ip);
			FileInputStream ele = new FileInputStream(System.getProperty("user.dir") + "/src/main/java/com/arm/arc/elements/locator.properties");
			prop.load(ele);
		} catch (FileNotFoundException e) {
			e.printStackTrace();	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	//checks config.properites file and verifies which browser is it that I want to use.
	public static void initialization() {
		
		String browserName = prop.getProperty("browser");
		
		if(browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + File.separator + "browserDriver" + File.separator
							+ "chromedriver.exe");
			driver = new ChromeDriver();
			
		}
		else if(browserName.equals("FF")){
			System.setProperty("webdriver.gecko.driver",
					System.getProperty("user.dir") + File.separator + "browserDriver" + File.separator
							+ "chromedriver.exe");
			driver = new FirefoxDriver();
		}
		
		/*e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver; */
		
	
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		
		//instead of hard coding the timeouts I'll create a TestUtil class and call the values from there.
		//alternatively timeouts could have been placed in the config.properites file.
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		
		//launches url specified in config.properties file
		driver.get(prop.getProperty("url"));
	}
	
}

	