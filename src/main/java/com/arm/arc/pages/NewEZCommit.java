package com.arm.arc.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.arm.arc.base.TestBase;
import com.paulhammant.ngwebdriver.ByAngular;

public class NewEZCommit extends TestBase {

	NewEZCommit()
	{
		        PageFactory.initElements(driver, this);
		    
	}
	// Read inputs from Config.properties file
		String sourceOrgname = prop.getProperty("sourceOrgname");
		String DestinationOrgname = prop.getProperty("DestinationOrgname");
		String QAAutoNonDXRepoName = prop.getProperty("QAAutoNonDXRepoName");
		String NonDXRepoBranchName = prop.getProperty("NonDXRepoBranchName");
		String ApexClass = prop.getProperty("ApexClass");
		String SelectedTab = prop.getProperty("SelectedTabByID");
		String revieweremail = prop.getProperty("revieweremail");
		String unixTimeStamp = String.valueOf(System.currentTimeMillis()/1000L);
		
	
	//All Locators are initialized here.
			String SFSourceOrgByID = prop.getProperty("SFSourceOrgByID");
			String SFSourceOrgInputByClasss = prop.getProperty("SFSourceOrgInputByClasss"); 
			String SFSourceOrgByClassName = prop.getProperty("SFSourceOrgByClassName");
			String AR_LoadingdivByID = prop.getProperty("AR_LoadingdivByID");
			String RepoDivByID = prop.getProperty("RepoDivByID");
			String RepoBranchDivByID = prop.getProperty("RepoBranchDivByID");
			
			String AutoDraftRadioBtnByID = prop.getProperty("AutoDraftRadioBtnByID");
			String SelectManuallyRadioBtnByID = prop.getProperty("SelectManuallyRadioBtnByID");
			String EZC_NewCommit_Packagexml = prop.getProperty("EZC_NewCommit_Packagexml");
			String EZC_NewCommit_Template = prop.getProperty("EZC_NewCommit_Template");
			
			String PVCommitRadioBtnByID = prop.getProperty("PVCommitRadioBtnByID");
			String DirectCommitRadioBtnByID = prop.getProperty("EZC_NewCommit_Template");
			
			String NextBtnByID = prop.getProperty("NextBtnByID");
			
			String metadatamemberByXpath = prop.getProperty("metadatamemberByXpath");
			
			String GenerateDiffCheckBoxById = prop.getProperty("GenerateDiffCheckBoxById");
			String RunSCACheckBoxByID = prop.getProperty("RunSCACheckBoxByID");
			String SCAOptionsSelectByID = prop.getProperty("SCAOptionsSelectByID");
			String ValidateDeployCheckBoxByID = prop.getProperty("ValidateDeployCheckBoxByID");
			String DestSFOrgByXpath = prop.getProperty("DestSFOrgByXpath");
			
			String commitLabelNameByID = prop.getProperty("commitLabelNameByID");
			String commitCommentNameByID = prop.getProperty("commitCommentNameByID");
			String ReviewerEmailByID = prop.getProperty("ReviewerEmailByID");
			
			
			String FinishBtnByID = prop.getProperty("FinishBtnByID");
			
			String ApproveRejectSelectByID = prop.getProperty("ApproveRejectSelectByID");
			
			String ReviewerCommentsByID = prop.getProperty("ReviewerCommentsByID");
			
			String PopupByID = prop.getProperty("PopupByID");
			
			
			
			
			
			WebDriverWait wait;
			Boolean ar_loading_icon; 
			List <String> folderTypeMetadata = Arrays.asList("Dashboard","Document","EmailTemplate","Report","PickList","StandardField");


			public String FillDataOnSubmitForValidation() throws InterruptedException
			{
				System.out.println("this is inside filling");
				//Thread.sleep(3000);
				WebElement generatediff = driver.findElement(By.id(GenerateDiffCheckBoxById));
				generatediff.click();
				
				WebElement runSCA = driver.findElement(By.id(RunSCACheckBoxByID));
				if(!(runSCA.isSelected())) {
					runSCA.click();
				}
				
				
				Select SCAOption = new Select(driver.findElement(By.id(SCAOptionsSelectByID)));
				SCAOption.selectByVisibleText("ApexPMD/Lint");
				
				WebElement validateOrg= driver.findElement(By.id(ValidateDeployCheckBoxByID));
				if(!(validateOrg.isSelected()))
				{
					validateOrg.click();
					//Thread.sleep(2000);
				}
				
				WebElement DestOrg= driver.findElement(By.xpath(DestSFOrgByXpath));
				  DestOrg.click(); 
				  WebElement DestOrgInput =
				  driver.findElement(By.className(SFSourceOrgInputByClasss));
				  DestOrgInput.sendKeys(DestinationOrgname); DestOrgInput.sendKeys(Keys.ENTER);
				  
				  String CommitComment = "PV-Commit"+unixTimeStamp;
				  WebElement commitlabel = driver.findElement(By.id(commitLabelNameByID));
				  commitlabel.sendKeys(CommitComment);
				  
				  WebElement commitcomment = driver.findElement(By.id(commitCommentNameByID));
				  commitcomment.sendKeys(CommitComment);
				  
				  WebElement emailinput = driver.findElement(By.id(ReviewerEmailByID));
				  emailinput.sendKeys(revieweremail);
				  emailinput.sendKeys(Keys.ENTER); 
				  wait = new WebDriverWait(driver, 15); wait.until(
							  ExpectedConditions.invisibilityOfElementLocated(By.id(AR_LoadingdivByID)));
				  WebElement finishbtn = driver.findElement(By.id(FinishBtnByID));
				  finishbtn.click();
				  return CommitComment;
					/*
					 * wait = new WebDriverWait(driver, 15); wait.until(
					 * ExpectedConditions.invisibilityOfElementLocated(By.id(AR_LoadingdivByID)));
					 * //Thread.sleep(10000);
					 */			}
	public String PerformNewEZCommit() throws InterruptedException
	{
		wait = new WebDriverWait(driver, 15);
		 wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(AR_LoadingdivByID)));
		 
		WebElement SFSourceOrg= driver.findElement(By.id(SFSourceOrgByID));
		  SFSourceOrg.click();
		  WebElement SFSourceOrgInput =  driver.findElement(By.className(SFSourceOrgInputByClasss));
		  SFSourceOrgInput.sendKeys(sourceOrgname);
		  SFSourceOrgInput.sendKeys(Keys.ENTER);
		WebElement RepoDiv =  driver.findElement(By.id(RepoDivByID));
		  RepoDiv.click();
		  WebElement RepoInput =  driver.findElement(By.className(SFSourceOrgInputByClasss));
		  RepoInput.sendKeys(QAAutoNonDXRepoName);
		  RepoInput.sendKeys(Keys.ENTER);
		
		 int attempts = 0;
		 while(attempts < 2) {
		        try {
		        	WebElement RepoBranch= driver.findElement(By.id(RepoBranchDivByID));
		        	RepoBranch.click(); 
		   		 
		        	WebElement RepoBranchInput =  driver.findElement(By.className(SFSourceOrgInputByClasss));
		        	RepoBranchInput.sendKeys(NonDXRepoBranchName);
		        	RepoBranchInput.sendKeys(Keys.ENTER);
		           
		            break;
		        } catch(Exception e) { System.out.println(e.getMessage());
		        }
		        attempts++;
		    }
		
		 WebElement fetchType = driver.findElement(By.id(SelectManuallyRadioBtnByID));
		 fetchType.click();
		 
		 WebElement commitType = driver.findElement(By.id(PVCommitRadioBtnByID));
		 commitType.click();
		 
		 WebElement NextBtn = driver.findElement(By.id(NextBtnByID));
		 NextBtn.click();
		 
		 List<String> apexList = new ArrayList<String>();
		 
		 apexList.add("A00000");
		 
		 List<String> triggerList = new ArrayList<String>();
		 triggerList.add("ApexTrigger1");
		 selectMetadata(ApexClass,apexList);
		 selectMetadata("ApexTrigger",triggerList);
		 //verifyMetadataInSelecteTab(ApexClass,apexList);
		 //verifyMetadataInSelecteTab("ApexTrigger",triggerList);
		 clickOnNextButton(); 
		 String commitComment = FillDataOnSubmitForValidation();
		 ApproveALabel(commitComment,"Approve");
		 System.out.println("this is returning.......");
		 
		
		return "abc123er";
	}
public boolean selectMetadata(String metadatType,List<String>metadatamembers)
{
		if(!(folderTypeMetadata.contains(metadatType)))
	{
		
		WebElement metadatatypespan = driver.findElement(By.xpath("//span[text()='"+metadatType+"']"));
		metadatatypespan.click();
		for(int i=0;i<metadatamembers.size();i++)
		{
			WebElement metadatamember = driver.findElement(By.xpath("//input[@metadatamember='"+metadatamembers.get(i)+"']"));
			  metadatamember.click(); 
			  System.out.println("Metadata Member Selected");
		}
		   return true;
		 
	}
	else
		System.out.println("The Selected is folder type, Dev in Progress");
		return false;
}

public boolean verifyMetadataInSelecteTab(String metadatType,List<String>metadatamembers) throws InterruptedException
{
	WebElement selectedtab = driver.findElement(By.id(SelectedTab));
	selectedtab.click();

	
	  WebElement metadatatypeheader = driver.findElement(By.xpath("//h2[normalize-space(text())='"+metadatType+"']")); 
	  Assert.assertTrue(metadatatypeheader.isDisplayed(),"Metadata header is not seen as expected");
	 
	
	  for(int i=0;i<metadatamembers.size();i++) 
	  { 
		  WebElement metadatamember = driver.findElement(By.xpath("//input[@metadatamember='"+metadatamembers.get(i)+"']"));
		  Assert.assertTrue(metadatamember.isDisplayed());
	      System.out.println("Metadata Member Selected"); 
	  }
	 
	return true;
}

public boolean clickOnNextButton() throws InterruptedException
{
	wait = new WebDriverWait(driver, 15); 
	 wait.until( 
			ExpectedConditions.elementToBeClickable(By.id("EZC_NextScreen")));
	WebElement nextBtn = driver.findElement(By.id("EZC_NextScreen"));
	System.out.println(nextBtn + "before clicking"); 
	nextBtn.click();
	System.out.println("next clicked");
	Thread.sleep(10000);
	return true;
}

public void ApproveALabel(String LabelName, String Action)
{
	
	WebElement labelName = driver.findElement(By.xpath("//label[@labelname='"+LabelName+"']//span"));
	//WebElement labelName = driver.findElement(By.cssSelector("//label[@labelname='"+LabelName+"'] .Conflicting-btn > span"));
	//wait = new WebDriverWait(driver, 15); wait.until(
		//	  ExpectedConditions.elementToBeClickable(labelName));
	 wait = new WebDriverWait(driver, 15); wait.until(
			  ExpectedConditions.invisibilityOfElementLocated(By.id(AR_LoadingdivByID)));												
	labelName.click();
	
	 wait = new WebDriverWait(driver, 15); wait.until(
			  ExpectedConditions.invisibilityOfElementLocated(By.id(AR_LoadingdivByID)));
	
	 Select action = new Select(driver.findElement(By.id(ApproveRejectSelectByID)));
	action.selectByValue(Action);
	if(Action.equalsIgnoreCase("reject"))
	{
		WebElement rejectComments = driver.findElement(By.id(ReviewerCommentsByID));
		rejectComments.sendKeys("Rejecting"+LabelName);
		rejectComments.sendKeys(Keys.TAB);
		rejectComments.sendKeys(Keys.ENTER);
		rejectComments.sendKeys(Keys.ENTER);
		WebElement popuMsg = driver.findElement(By.id(PopupByID));
		String RejectMsg = popuMsg.getText();
		Assert.assertTrue(RejectMsg.contains("label has been rejected")); 
		if(driver.findElement(By.xpath("//tr[@labelname='"+LabelName+"' and @status='"+Action+"']")) != null)
		{
			Assert.assertTrue(driver.findElement(By.xpath("//tr[@labelname='"+LabelName+"'and @status='"+Action+"']")).isDisplayed());
		}
		}
	else if(Action.equalsIgnoreCase("approve"))
	{
		System.out.println("inside approve");
		WebElement approveComments = driver.findElement(By.id(ReviewerCommentsByID));
		approveComments.sendKeys("Approving"+LabelName);
		approveComments.sendKeys(Keys.TAB);
		approveComments.sendKeys(Keys.ENTER);
		approveComments.sendKeys(Keys.ENTER); 
		WebElement popuMsg = driver.findElement(By.id(PopupByID));
		String ApprovetMsg = popuMsg.getText();
		Assert.assertTrue(ApprovetMsg.contains("label has been approved successfully"));
		if(driver.findElement(By.xpath("//tr[@labelname='"+LabelName+"'and @status='Completed']")) != null)
		{
			Assert.assertTrue(driver.findElement(By.xpath("//tr[@labelname='"+LabelName+"'and @status='Completed']")).isDisplayed());
		}
		
		WebElement commitbtn = driver.findElement(By.xpath("//label[@labelname='"+LabelName+"']//span"));
		commitbtn.click();
		commitbtn.sendKeys(Keys.TAB);
		commitbtn.sendKeys(Keys.ENTER);
	}
	
}

}
