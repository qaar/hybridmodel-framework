package com.arm.arc.pages;

import com.arm.arc.base.TestBase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage extends TestBase {
	
	
	//All Locators are initialized here.
		String newCommitTemplateBtn = prop.getProperty("newCommitTemplteByID");
		String versionControlModuleXpath = prop.getProperty("versionControlModuleXpath");
		String AR_LoadingdivByID = prop.getProperty("AR_LoadingdivByID");
		String newEZCommitButton = prop.getProperty("newEZCommitButtonByID");
		String newMergeButton = prop.getProperty("newMergeButtonByID");
		
		
		
		/*
		 * WebElement ezcommitbtn = driver.findElement(By.id(newEZCommitButton));
		 */
		WebDriverWait wait;
		Boolean ar_loading_icon;

    //LoginPage constructor will initialize LoginPage objects/variables above using WebDriver
    public HomePage() {
        PageFactory.initElements(driver, this);
    } 


    public Object navigateToParticularModule(String moduleName) throws InterruptedException {

    	switch(moduleName)
    	{
    	case "VC":
    		/* ExpectedConditions.invisibilityOfElementLocated(By.id("AR_LoadingdivByID")));
			 Assert.assertTrue(ar_loading_icon);  */
    		 WebElement vc_module= driver.findElement(By.xpath(versionControlModuleXpath));
    		vc_module.click();
    		return new VCModule();
    	default: 
    		System.out.print("this is default") ;
    		return new NewEZCommit();
    	}	
    }
    
    	
    
}
