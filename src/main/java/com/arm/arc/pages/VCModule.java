package com.arm.arc.pages;

import com.arm.arc.base.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class VCModule extends TestBase {
    //LoginPage constructor will initialize LoginPage objects/variables above using WebDriver
    public VCModule() {
        PageFactory.initElements(driver, this);
    }

/* this is testing comment
public VCModule createvc() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement element1 = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".icon-vc-icon2")));
    driver.findElement(By.cssSelector(".icon-vc-icon2")).click();
    driver.findElement(By.id("VC_Layout_Head_NewEZCommit")).click();
    driver.findElement(By.id("select2-EZC_NewCommit_SourceOrg-container")).click();
    driver.findElement(By.cssSelector(".ui-button-text")).click();
    driver.findElement(By.id("select2-EZC_NewCommit_SourceOrg-container")).click();
    driver.findElement(By.id("select2-EZC_NewCommit_SourceOrgAuthor-container")).click();
    driver.findElement(By.id("select2-EZC_NewCommit_Repository-container")).click();
    driver.findElement(By.id("select2-EZC_NewCommit_Branch-container")).click();
    driver.findElement(By.id("EZC_NextScreen")).click();
    driver.findElement(By.cssSelector(".EZC_NewCommit_MetadataTypeToggleDiv:nth-child(4) .EZC_MetadataMemberChkBoxTr:nth-child(2) .EZC_MetadataMemberChkBox")).click();
    driver.findElement(By.id("EZC_NextScreen")).click();
    driver.findElement(By.id("EZC_Validate_GenerateCodeDiff")).click();
    driver.findElement(By.id("EZC_Validate_LabelName")).click();
    driver.findElement(By.id("EZC_Validate_LabelName")).sendKeys("selencomit1");
    driver.findElement(By.id("EZC_Validate_CommitComment")).click();
    driver.findElement(By.id("EZC_Validate_CommitComment")).sendKeys("selenemcommit1");
    driver.findElement(By.id("EZC_Validate_LabelName")).click();
    driver.findElement(By.id("EZC_Validate_LabelName")).sendKeys("selencomitlabel1");
    driver.findElement(By.id("EZC_Validate_ReviewerEmail-flexdatalist")).click();
    driver.findElement(By.id("EZC_Validate_ReviewerEmail-flexdatalist")).sendKeys("madhusudh");
    driver.findElement(By.cssSelector(".item-label")).click();
    driver.findElement(By.id("EZC_Finish")).click();
    driver.findElement(By.cssSelector(".Inprogress-btn > span")).click();
    driver.findElement(By.id("reviewSCMCommit")).click();
    driver.findElement(By.id("ScmDetails_ReviewStatus")).click();
    {
        WebElement dropdown = driver.findElement(By.id("ScmDetails_ReviewStatus"));
        dropdown.findElement(By.xpath("//option[. = 'Approve']")).click();
    }
    driver.findElement(By.cssSelector(".ui-state-hover > .ui-button-text")).click();
    {
        WebElement element = driver.findElement(By.cssSelector(".ui-dialog:nth-child(20) .ui-button-text"));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).perform();
    }
    driver.findElement(By.cssSelector(".ui-state-hover > .ui-button-text")).click();
    {
        WebElement element = driver.findElement(By.tagName("body"));
        Actions builder = new Actions(driver);
        builder.moveToElement(element, 0, 0).perform();
    }
    driver.findElement(By.id("ScmDetails_RePush")).click();
    driver.findElement(By.cssSelector(".ui-state-hover > .ui-button-text")).click();
    driver.findElement(By.id("ScmDetailsRefresh")).click();
    driver.findElement(By.cssSelector(".ScmInfo_label_btnInfo")).click();
    return new VCModule();
}*/
   
   
// Read inputs from Config.properties file
	String enterUsername = prop.getProperty("username");
	String enterPassword = prop.getProperty("password");
	String invalidEmailID = prop.getProperty("invalid_username");
	String errorMsgText = prop.getProperty("forgotPasswordEmptyEmailErrorMsg");
	String invalidLoginMsg = prop.getProperty("invalidLogin");
	String alertMsg = prop.getProperty("forgotPasswordWithValidEmailID");
	String alertMsgWithValid = prop.getProperty("forgotPasswordWithInValidEmailID");
	String AR_LoadingdivByID = prop.getProperty("AR_LoadingdivByID");

	
	//All Locators are initialized here.
			String newCommitTemplateBtn = prop.getProperty("newCommitTemplteByID");
			String versionControlModuleXpath = prop.getProperty("versionControlModuleXpath");
			
			String newEZCommitButton = prop.getProperty("newEZCommitButtonByID");
			String newMergeButton = prop.getProperty("newMergeButtonByID");
			String newMergeRequestButton = prop.getProperty("newMergeRequsetButtonByID");
			
			
			WebDriverWait wait;
			Boolean ar_loading_icon;
		

	public Object ClickOnActionInModule(String ActionName)
	{
		switch(ActionName)
    	{
    	case "committemplate":
    		WebElement committemplatebtn = driver.findElement(By.id(newCommitTemplateBtn));
    		committemplatebtn.click();
    		return new NewEZCommit();
    		 
    	case "ezcommit":
    		wait = new WebDriverWait(driver, 5);
    		ar_loading_icon=wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("AR_LoadingdivByID")));
    		Assert.assertTrue(ar_loading_icon);
    		WebElement ezcommitbtn = driver.findElement(By.id(newEZCommitButton));
    		ezcommitbtn.click();
    		System.out.print("z coomit 107 line");
    		return new NewEZCommit();
    		
    	case "ezmerge":
    		WebElement mergebtn = driver.findElement(By.id(newMergeButton));
    		mergebtn.click();
    		return new NewEZCommit();
    		
    	case "mergerequest":
    		WebElement mergerequestbtn = driver.findElement(By.id(newMergeRequestButton));
    		mergerequestbtn.click();
    		return new NewEZCommit();
    		
    	default:
    		return new NewEZCommit();
    		
    	}
	}

}
