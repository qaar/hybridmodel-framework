package com.arm.arc.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.arm.arc.base.TestBase;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class LoginPage extends TestBase {

	// Read inputs from Config.properties file
	String enterUsername = prop.getProperty("username");
	String enterPassword = prop.getProperty("password");
	String invalidEmailID = prop.getProperty("invalid_username");
	String errorMsgText = prop.getProperty("forgotPasswordEmptyEmailErrorMsg");
	String invalidLoginMsg = prop.getProperty("invalidLogin");
	String alertMsg = prop.getProperty("forgotPasswordWithValidEmailID");
	String alertMsgWithValid = prop.getProperty("forgotPasswordWithInValidEmailID");


	//All Locators are initialized here.
	String userNameField = prop.getProperty("arcloginFieldByID");
	String passwordField = prop.getProperty("arcPasswordFieldByID");
	String loginButton = prop.getProperty("arcLoginButtonByID");
	String forgotPwd = prop.getProperty("forGotPasswordButtonByID");
	String errorMsgLocator = prop.getProperty("invalidForgotPasswordErrorMessageByID");
	String alertMsgForForgotPwd = prop.getProperty("validForgotPasswordMessageByID");
	String alertMsgForgotPwdWithValidUName = prop.getProperty("validForgotPasswordMessageByID");

	//LoginPage constructor will initialize LoginPage objects/variables above using WebDriver
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	 
	//returns title of the page
	public String validateLoginPageTitle() {
		return driver.getTitle();	
	}

	public void enterUserName() {
		WebElement uname = driver.findElement(By.id(userNameField));
		uname.clear();
		uname.sendKeys(enterUsername);
	}

	public void setInvalidEmailID() {
		WebElement uname = driver.findElement(By.id(userNameField));
		uname.clear();
		uname.sendKeys(invalidEmailID);
	}

	public void enterPassword() {
		WebElement upwd = driver.findElement(By.id(passwordField));
		upwd.clear();
		upwd.sendKeys(enterPassword);
	}

	public void clickLoginButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(loginButton)));
		assertTrue(element.isEnabled());
		element.click();
	}

	public LoginPage verifyForgotPasswordWithInValidEmail() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(forgotPwd)));
		assertTrue(element.isDisplayed());

		WebElement upwd = driver.findElement(By.id(passwordField));
		upwd.clear();
		element.click();
		String actualmsg = this.verifyForgotPasswordWithEmptyEmailErrorMsg();
		assertEquals("Error Message is " , actualmsg, errorMsgText);

		this.setInvalidEmailID();
		element.click();
		String actualmsg1 = this.verifyForgotPasswordAlertMsg();
		assertEquals("Error Message is " , alertMsg, actualmsg1);
		this.closeAlertBox();
		driver.navigate().refresh();
		return new LoginPage();
	}

	public LoginPage verifyForgotPasswordWithProperValidEmail() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(forgotPwd)));
		assertTrue(element.isDisplayed());

		this.setInvalidEmailID();
		this.clickLoginButton();
		String actualmsg = this.verifyForgotPasswordWithEmptyEmailErrorMsg();
		assertEquals("Error Message is " , actualmsg, invalidLoginMsg);


		this.enterUserName();
		element.click();
		String actualmsg1 = this.verifyForgotPasswordAlertMsg();
		assertEquals("Error Message is " , alertMsgWithValid, actualmsg1);
		this.closeAlertBox();
		driver.navigate().refresh();
		return new LoginPage();
	}

	public void closeAlertBox() {
		driver.findElement(By.cssSelector(".ui-button-text")).click();
	}

	public String verifyForgotPasswordAlertMsg() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(alertMsgForgotPwdWithValidUName)));
		return driver.findElement(By.id(alertMsgForgotPwdWithValidUName)).getText();
	}

	public String verifyForgotPasswordWithEmptyEmailErrorMsg() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(errorMsgLocator)));
		return driver.findElement(By.id(errorMsgLocator)).getText();
	}

	public String verifyForgotPasswordWithValidEmailErrorMsg() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.id(alertMsgForForgotPwd)));
		return driver.findElement(By.id(alertMsgForForgotPwd)).getText();
	}


	public HomePage login() {
		enterUserName();
		enterPassword();
		clickLoginButton();
		return new HomePage();
	}
	
}	